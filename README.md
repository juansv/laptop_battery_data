# Laptop Battery Data

It is a python3 class for easy laptop battery data reading.

It reads on `/sys/class/power_supply/BATX` where *X* is a number assigned by the
system. It returns all data in python coherent variables, I mean, it
returns float, int, str, etc. depending on data nature.

For further info:  
<https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power>  
<https://www.kernel.org/doc/html/latest/power/power_supply_class.html>

They are made to program on GNU/Linux systems.

### Installing

Copy the `.py` and just simply use it as a module:

`from laptopbatterydata import LaptopBatteryData`

#### Dependencies

It use `Path` from `pathlib` module.

### Use

It works by creating an object which represent your battery. It is necessary the directory name of your battery in `/sys/class/power_supply/` which normally is *BAT0* or *BAT1* or similar.

Optionally, the submultiple used for measures could be given. **None** means no submultiple and methods affected return measures in the International System. It is accepted **"m"** for *"mili-"* and **"u"** for *"micro-"* and nothing else. *None* is the default.

Let's see an example:

Battery directory is *BAT0* and we want all technical units in *micro-*

~~~
from laptopbatterydata import LaptopBatteryData

mybattery = LaptopBatteryData("BAT0", "u")
~~~

Battery directory is *BAT1* and we want all technical units as it, no submultiple

~~~
from laptopbatterydata import LaptopBatteryData

mybattery = LaptopBatteryData("BAT1")
~~~

And so on...

Now, we want to read the present charge level in %:

~~~
print(mybattery.chargepercentage())
~~~

The same value but in Ah (or mAh or uAh):

~~~
charge = mybattery.chargeAh()
~~~

The manufacturer name:

~~~
print(mybattery.manufacturer())
~~~

Please, take notice of:

- According to [kernel documentation](https://www.kernel.org/doc/html/latest/power/power_supply_class.html): *all voltages, currents, charges, energies, time and temperatures in µV, µA, µAh, µWh, seconds and tenths of degree Celsius*. As a result, when you ask for *micro-* it gives back integrer. But if you ask for *mili-* or no submultiple, you get an float.
- To get the info you want, it is necessary that your system [has the file](https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power) in `/sys/class/power_supply/BATX`. It returns an exception if there is no file.

#### List of methods

| FILE | METHOD | RETURNS | UNIT |
| --- | --- | --- | --- |
| manufacturer | manufacturer | str | text |
| serial_number | serialnumber | str | text |
| model_name | modelname | str | text |
| type | batterytype | str | text |
| technology | batterytech | str | text |
| charge_full_design | chargefulldesign | int | % |
| charge_empty_design | chargeemptydesign | int | % |
| voltage_max_design | voltagemaxdesign | int/float | V |
| voltage_min_design | voltagemindesign | int/float | V |
| temp_max | tempmax | int | C |
| temp_min | tempmin | int | C |
| voltage_max | voltagemax | int/float | V |
| voltage_min | voltagemin | int/float | V |
| current_max | currentmax | int/float | A |
| charge_control_limit_max | chargelimitmax | int/float | Ah |
| charge_term_current | chargeendcurrent | int/float | Ah |
| charge_type | chargetype | str | text |
| charge_behaviour | chargebehaviour | str | text |
| temp_alert_max | tempmaxalert | int | C |
| temp_alert_min | tempminalert | int | C |
| capacity_alert_max | capmaxalert | int | % |
| capacity_alert_min | capminalert | int | % |
| charge_control_limit | chargelimit | int/float | Ah |
| charge_control_start_threshold | chargestart | int | % |
| charge_control_end_threshold | chargeend | int | % |
| capacity | chargepercentage | int | % |
| charge_now | chargeAh | int/float | Ah |
| current_now | current | int/float | A |
| voltage_now | voltage | int/float | V |
| current_avg | currentavg | int/float | A |
| voltage_avg | voltageavg | int/float | V |
| temp | temperature | int | C |
| precharge_current | prechargecurrent | int/float | Ah |
| charge_full | chargefullactual | int/float | Ah |
| charge_empty | chargeemptyactual | int/float | Ah |
| capacity_error_margin | capacityerror | int | % |
| cycle_count | cyclecount | int | counter |
| present | present | bool | true/false |
| status | status | str | text |
| status | charging | bool | true/false |
| status | discharging | bool | true/false |
| status | notcharging | bool | true/false |
| status | full | bool | true/false |
| capacity_level | capacitylevel | str | text |
| health | health | str | text |

### Technical

It is made for python 3.

### Versions

#### 1.1.0

Some methods added for checking directly the status file value.

### Author and license

Copyright 2022 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org).

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licences/gpl.html>.

