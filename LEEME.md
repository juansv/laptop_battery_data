# Laptop Battery Data

Esto es una clase para python3 para leer datos técnicos de la batería de un portátil.

Lee en el directorio `/sys/class/power_supply/BATX`, donde *X* es el número que la batería tiene asignado. Devuelve los datos en un formato de variable coherente y manejable por un programa python, es decir, devuelve float, int, str, etc. según la naturaleza del dato.

Para más info:  
<https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power>  
<https://www.kernel.org/doc/html/latest/power/power_supply_class.html>

Solo sirve para programar en sistemas GNU/Linux.

### Instalación

Copia el archivo `.py` y úsalo como un módulo:

`from laptopbatterydata import LaptopBatteryData`

#### Dependencias

Usa `Path` del módulo `pathlib`.

### Uso

Funciona creando un objeto que representa tu batería. Es necesario indicar el nombre que la batería tiene en `/sys/class/power_supply/` que normalmente es *BAT0* o *BAT1* o similar.

También se le puede entregar, opcionalmente, el submúltiplo de las unidades de medida (mili, micro) que se usará con los valores numéricos. Por defecto devuelve los datos sin submúltiplo, que es la opción **None** También acepta **"m"** para *"mili-"* y **"u"** para *"micro-"*.

Veamos un ejemplo:

El directorio de la batería es *BAT0* y queremos la información técnica en submúltiplo *micro-*

~~~
from laptopbatterydata import LaptopBatteryData

mybattery = LaptopBatteryData("BAT0", "u")
~~~

Ahora el directorio de la batería es *BAT1* y queremos las unidades tal cual, sin submúltiplo

~~~
from laptopbatterydata import LaptopBatteryData

mybattery = LaptopBatteryData("BAT1")
~~~

Y así sucesivamente...

Ahora vamos a leer el valor actual del nivel de carga en %:

~~~
print(mybattery.chargepercentage())
~~~

El mismo valor, pero en Ah (o mAh o uAh):

~~~
charge = mybattery.chargeAh()
~~~

El nombre del fabricante:

~~~
print(mybattery.manufacturer())
~~~

Por favor, tenga presente que:

- Según la [documentación del kernel](https://www.kernel.org/doc/html/latest/power/power_supply_class.html): *all voltages, currents, charges, energies, time and temperatures in µV, µA, µAh, µWh, seconds and tenths of degree Celsius* (*todos los voltages, corrientes, cargas, energías, unidades de tiempo y temperaturas se dan en, respectivamente, µV, µA, µAh, µWh, segundos y décimos de grado Celsius*). Por lo tanto, cuando se pregunta por *micro-* los métodos devuelven un entero (int). Pero si se pregunta por *mili-* o sin submúltiplos, obtendrá un float.
- Para obtener la información es necesario que [exista el archivo](https://www.kernel.org/doc/Documentation/ABI/testing/sysfs-class-power) correspondiente en el directorio `/sys/class/power_supply/BATX`. Si no existe el archivo, devuelve una excepción.

#### Lista de métodos

| ARCHIVO | MÉTODO | DEVUELVE | UNIDADES |
| --- | --- | --- | --- |
| manufacturer | manufacturer | str | text |
| serial_number | serialnumber | str | text |
| model_name | modelname | str | text |
| type | batterytype | str | text |
| technology | batterytech | str | text |
| charge_full_design | chargefulldesign | int | % |
| charge_empty_design | chargeemptydesign | int | % |
| voltage_max_design | voltagemaxdesign | int/float | V |
| voltage_min_design | voltagemindesign | int/float | V |
| temp_max | tempmax | int | C |
| temp_min | tempmin | int | C |
| voltage_max | voltagemax | int/float | V |
| voltage_min | voltagemin | int/float | V |
| current_max | currentmax | int/float | A |
| charge_control_limit_max | chargelimitmax | int/float | Ah |
| charge_term_current | chargeendcurrent | int/float | Ah |
| charge_type | chargetype | str | text |
| charge_behaviour | chargebehaviour | str | text |
| temp_alert_max | tempmaxalert | int | C |
| temp_alert_min | tempminalert | int | C |
| capacity_alert_max | capmaxalert | int | % |
| capacity_alert_min | capminalert | int | % |
| charge_control_limit | chargelimit | int/float | Ah |
| charge_control_start_threshold | chargestart | int | % |
| charge_control_end_threshold | chargeend | int | % |
| capacity | chargepercentage | int | % |
| charge_now | chargeAh | int/float | Ah |
| current_now | current | int/float | A |
| voltage_now | voltage | int/float | V |
| current_avg | currentavg | int/float | A |
| voltage_avg | voltageavg | int/float | V |
| temp | temperature | int | C |
| precharge_current | prechargecurrent | int/float | Ah |
| charge_full | chargefullactual | int/float | Ah |
| charge_empty | chargeemptyactual | int/float | Ah |
| capacity_error_margin | capacityerror | int | % |
| cycle_count | cyclecount | int | counter |
| present | present | bool | true/false |
| status | status | str | text |
| status | charging | bool | true/false |
| status | discharging | bool | true/false |
| status | notcharging | bool | true/false |
| status | full | bool | true/false |
| capacity_level | capacitylevel | str | text |
| health | health | str | text |

### Tecnología

Está hecho para python3.

### Versiones

#### 1.1.0

Se añaden métodos para consultar directamente los posibles valores del archivo status.

### Autor y licencia

Copyright 2022 [Juan Antonio Silva Villar](mailto:juan.silva@disroot.org).

License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licences/gpl.html>.

